const resArr = []
var cntr
setInterval(function(){
  cntr = (cntr - 1)||30
  resArr.forEach(res => {
    res.write((''+cntr).padStart(2, '0'))
  });
}, 1000)
const index = `<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>
<h1 id="cntr"></h1>
<script>
var ajax = new XMLHttpRequest()
ajax.onreadystatechange = function(){
  //if (ajax.readyState == 3 || ajax.readyState == 4) {
    if (ajax.response) {
      cntr.innerHTML = ajax.response.slice(-2)
    }
  //}
}
ajax.open('GET', '/cntr')
ajax.send()
</script>
</body></html>`
require('http').createServer(function(req, res){
  ({
    '/': function(){ res.end(index) },
    '/cntr': function(){
      res.setHeader('Content-Type', 'text/stream')
      resArr.push(res)
      res.on('close', function(){
        resArr.splice(resArr.findIndex(_res => res == _res), 1)
      })
    }
  }[req.url]||function(){res.end()})()
}).listen(8080)